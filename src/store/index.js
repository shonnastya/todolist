import Vue from 'vue'
import Vuex from 'vuex'

import db from '../firebase/init'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    tasks: null
  },
  mutations: {},
  actions: {
    async updateTasks({ state }) {
      const sh = await db.collection('tasks').get()
      state.tasks = sh.docs.map(doc => ({
        id: doc.id,
        ...doc.data()
      }))
    }
  },
  modules: {}
})
