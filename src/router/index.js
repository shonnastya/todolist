import Vue from 'vue'
import Router from 'vue-router'

import Main from './components/Main.vue'
import Edit from './components/Edit.vue'

Vue.use(Router)

const router = new Router({
  routes: [
    { path: '*', component: Main },
    { path: '/', name: 'name', component: Main },
    { path: '/edit/:id', name: 'Edit', component: Edit }
  ],
  mode: 'history'
})

export default router
