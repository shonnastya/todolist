import * as firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/storage'

var Config = {
  apiKey: 'AIzaSyD6SaJdQEMM6NzWMJKdz0nMfCpEWHdm9FQ',
  authDomain: 'todo-list-25935.firebaseapp.com',
  databaseURL: 'https://todo-list-25935.firebaseio.com',
  projectId: 'todo-list-25935',
  storageBucket: 'todo-list-25935.appspot.com',
  messagingSenderId: '928204960126',
  appId: '1:928204960126:web:38d5da7ae1bda25188432f'
}

const firebaseApp = firebase.initializeApp(Config)

export const storage = firebase.storage

export default firebaseApp.firestore()
